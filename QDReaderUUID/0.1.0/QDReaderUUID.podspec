#
# Be sure to run `pod lib lint QDReaderUUID.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'QDReaderUUID'
  s.version          = '0.1.0'
  s.summary          = 'QDReaderUUID is a UUID+Keychian Module.'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = <<-DESC
TODO: Add long description of the pod here.
                       DESC

  s.homepage         = 'https://gitlab.com/Atlas19/QDReaderUUID'
  # s.screenshots     = 'www.example.com/screenshots_1', 'www.example.com/screenshots_2'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'Dylan Yang' => 'yangyuxin@yuewen.com' }
  s.source           = { :git => 'https://gitlab.com/Atlas19/QDReaderUUID.git', :tag => s.version.to_s }

  s.ios.deployment_target = '6.0'
  s.source_files = 'QDReaderUUID/Classes/**/*'
  s.public_header_files = 'QDReaderUUID/Classes/**/QDUUID.h'
  s.frameworks = 'Security'
end
